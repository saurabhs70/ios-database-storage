//
//  imgViewController.swift
//  Cruaapp
//
//  Created by saurabh-pc on 09/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

protocol imgviewdel:class {
    func getval(stringicon:String?)
}
class imgViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    weak var delegateimg : imgviewdel?
    var lists=[Any]()
    
    @IBAction func btndisnissClicked(_ sender: Any) {
        dismisswithName(str:"user")

       // self.navigationController?.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var tblimg: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let iconimg:Icon = Icon(stringofname: "",stringImage: "")
       lists = iconimg.setup()!
        tblimg.reloadData()
        
//lists=
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:imgcellTableViewCell=tableView.dequeueReusableCell(withIdentifier: "imgcell", for: indexPath) as! imgcellTableViewCell
        let iconobj:Icon = lists[indexPath.row] as! Icon
        //   print( (minister.mname) ?? "raju")
        
        let title=cell.lblname as UILabel
        title.text=iconobj.stringname
        var imgplot = cell.imgcell as UIImageView
      imgplot =  imgplot.circleImg(uiimg: imgplot, stringname: iconobj.stringImg!)
//        imgplot.layer.cornerRadius = imgplot.frame.size.width/2
//        imgplot.clipsToBounds = true
//         imgplot.image=UIImage(named:iconobj.stringImg!)!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  print("\(indexPath.row)")
        let iconObj:Icon = lists[indexPath.row] as! Icon
        dismisswithName(str: iconObj.stringImg!)
        //ministerObj=minister//lists[indexPath.row] as? Minister
       // delegateimg?.getval(stringicon: iconObj.stringImg)

        //performSegue(withIdentifier: "modify", sender: self)
    }


    func dismisswithName(str:String) {
        _=self.navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
        delegateimg?.getval(stringicon:str)

    }
}

