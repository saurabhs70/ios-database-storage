//
//  ViewController.swift
//  Cruaapp
//
//  Created by saurabh-pc on 05/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnplistclicked(_ sender: Any) {
        performSegue(withIdentifier: "plist", sender: self)

    }
    @IBAction func btnWebserviceclicked(_ sender: Any) {
        performSegue(withIdentifier: "webservice", sender: self)
        

    }
    @IBAction func btnSqlitclicked(_ sender: Any) {
    }
    @IBAction func btnCoreDataClicked(_ sender: Any) {
        performSegue(withIdentifier: "coredata", sender: self)
       //plist

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="coredata" {
            let obj = segue.destination as! CrudListViewController
             obj.mode=segue.identifier//sender as! Int?
        }
        else if  segue.identifier=="plist"
        {
            let obj = segue.destination as! CrudListViewController
             obj.mode=segue.identifier
            
        }
        else{
                let obj = segue.destination as! MoreViewController
                obj.stringmode=segue.identifier
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

