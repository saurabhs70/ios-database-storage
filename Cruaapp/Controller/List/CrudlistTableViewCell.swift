//
//  CrudlistTableViewCell.swift
//  Cruaapp
//
//  Created by saurabh-pc on 09/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class CrudlistTableViewCell: UITableViewCell {

    @IBOutlet weak var lbldesc: UITextView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imguser: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
