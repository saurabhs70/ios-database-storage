//
//  ModifyListViewController.swift
//  Cruaapp
//
//  Created by saurabh-pc on 05/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class ModifyListViewController: UIViewController, imgviewdel {

    var stringmode:String?
    var ministerObj:Minister?
    var stringimg:String=""
    
    @IBOutlet weak var btn: UIButton!
    @IBAction func AddimgClicked(_ sender: Any) {
        performSegue(withIdentifier: "imgpresent", sender: self)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="imgpresent" {
            let obj = segue.destination as! imgViewController
        obj.delegateimg=self//sender as! Int?
        }
    }
    func getval(stringicon: String?) {
       print(stringicon!)
        stringimg=stringicon!;
        if (stringicon?.characters.count)!>1 {
            btn.setImage(UIImage(named:stringicon!), for: .normal)
        }
        else
        {
        btn.setImage(UIImage(named:"user.png"), for: .normal)

        }

    }
    @IBOutlet weak var imguser: UIImageView!
    @IBOutlet weak var txttitle: UITextField!
    
    @IBOutlet weak var btnmodify: UIButton!
    @IBOutlet weak var txtdesc: UITextView!
    
    @IBAction func btnmodifyClicked(_ sender: Any) {
        if stringmode=="coredata" {
            coreData();
        }
        else if stringmode=="plist"
        {
            plist()
        }
    }
    override func viewDidLoad() {
        if let obj = ministerObj?.mimage{
            btn.setImage(UIImage(named:obj), for: .normal)
        }
        else
        {
        btn.setImage(UIImage(named: "user.png"), for: .normal)
        }
        super.viewDidLoad()
       // print(ministerObj);
txttitle.text=ministerObj?.mname
txtdesc.text=ministerObj?.mabout
        if ((ministerObj?.mid) != nil)
        {
        btnmodify.setTitle("Update", for: .normal)
        }
        else{
            btnmodify.setTitle("Add", for: .normal)

        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func coreData() {
        if ((ministerObj?.mid) != nil)
        {
            
            let boolv = DBManager.sharedInstance.updateMinister(ministerObj: ministerObj!, stringname: txttitle.text!, stringabout: txtdesc.text) as Bool
            if boolv {
                //print("added")
                _ = self.navigationController?.popViewController(animated: true)
            }
            else
            {
               // print("not added")
            }
        }
        else
        {
            //print("not added")addToMinister(stringName: txttitle.text!, stringDescription: txtdesc.text
            let boolv = DBManager.sharedInstance.addToMinister(stringName: txttitle.text!, stringDescription: txtdesc.text, stringimg: stringimg) as Bool
            if boolv {
                //print("added")
                _ = self.navigationController?.popViewController(animated: true)
            }
            else
            {
                //print("not added")
            }
            
        }

    }
    func plist() {
         if ((ministerObj?.mid) != nil) {
            let boolforupdate = DBPlist.sharedInstance.updateList(stringname: txttitle.text!, ministerobj: ministerObj!, stringaddress: txtdesc.text)
            if boolforupdate {
            _ = self.navigationController?.popViewController(animated: true)
            }
            
        }
        else
         {
            //var st:String?
            //print(stringimg!)
            
            
            let boolval = DBPlist.sharedInstance.addToDatabase(stringname: txttitle.text!, stringinfo: txtdesc.text, stringimgnam: stringimg) as Bool
        if boolval {
           // print("added")
           _ = self.navigationController?.popViewController(animated: true)
        }
        else {
            //print("not added")
        }
        }
    }
}
