//
//  CrudListViewController.swift
//  Cruaapp
//
//  Created by saurabh-pc on 05/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class CrudListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var lists=[AnyObject]();
    var ministerObj:Minister?
    var mode:String?
    @IBAction func btnModifyClicked(_ sender: Any) {
        ministerObj=nil
        
        performSegue(withIdentifier: "modify", sender: self)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="modify" {
        let obj = segue.destination as! ModifyListViewController
            obj.stringmode=mode!
          //  print( (ministerObj?.mname) ?? "raju")
             obj.ministerObj=ministerObj//sender as! Int?
        }
//        else if  segue.identifier=="liststudentsid"
//        {
//            //let obj = segue.destination as! listStudentsViewController
//            // obj.listsarr=arrlist
//            
//        }
    }

    @IBOutlet weak var tbllist: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
     self.navigationItem.title=mode
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if mode=="coredata" {
            lists=DBManager.sharedInstance.getLists(nameofenity: "Minister")! as [AnyObject]

        }
        else if mode=="plist"
        {
            print("mia re\(DBPlist.sharedInstance.loadData())")
        lists=DBPlist.sharedInstance.loadData()! as  [AnyObject]
        }
        print("\n\n\n\nobly val\(lists)")
        DispatchQueue.main.async {
            self.tbllist.estimatedRowHeight=100
            self.tbllist.rowHeight=UITableViewAutomaticDimension
            self.tbllist.reloadData()

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CrudlistTableViewCell=tableView.dequeueReusableCell(withIdentifier: "listcell", for: indexPath) as! CrudlistTableViewCell
        let minister:Minister = lists[indexPath.row] as! Minister
     //   print( (minister.mname) ?? "raju")

        let txtview = cell.lbldesc as UITextView
         txtview.text=minister.mabout//lists[indexPath.row]
        let title=cell.lbltitle as UILabel
        title.text=minister.mname
        
        var imgplot = cell.imguser as UIImageView
        imgplot=imgplot.circleImg(uiimg: imgplot, stringname: "")
        if let valfind=minister.mimage {
            imgplot=imgplot.circleImg(uiimg: imgplot, stringname: valfind)
        }
        else
        {
            imgplot=imgplot.circleImg(uiimg: imgplot, stringname: "user.png")
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  print("\(indexPath.row)")
        let minister:Minister = lists[indexPath.row] as! Minister

        ministerObj=minister//lists[indexPath.row] as? Minister
        
        performSegue(withIdentifier: "modify", sender: self)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
////            let alert = UIAlertController(title: "", message: "Edit list item", preferredStyle: .alert)
////            alert.addTextField(configurationHandler: { (textField) in
////                textField.text = self.lists[indexPath.row] as? String
////            })
////            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (updateAction) in
////                self.lists[indexPath.row] = alert.textFields!.first!.text! as AnyObject
////                self.tbllist.reloadRows(at: [indexPath], with: .fade)
////            }))
////            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
////            self.present(alert, animated: false)
//        })
//        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
            if self.mode=="coredata"
            {
                let minister:Minister = self.lists[indexPath.row] as! Minister

                let checkdelete=DBManager.sharedInstance.deleteFromModel(ministerObj: minister)
                if checkdelete
                {
                    self.lists.remove(at: indexPath.row)
                    tableView.reloadData()
                }
            }
            if self.mode=="plist"
            {
                                let minister:Minister = self.lists[indexPath.row] as! Minister
              let refresh =  DBPlist.sharedInstance.deleteList(intId: minister.mid)
                if refresh
                {
                    self.lists.remove(at: indexPath.row)
                    tableView.reloadData()
                }
            }
          //  self.lists.remove(at: indexPath.row)
           // tableView.reloadData()
        })
        
       // return [deleteAction, editAction]
                return [deleteAction]
    }
}
