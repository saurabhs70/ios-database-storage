//
//  MoreViewController.swift
//  Cruaapp
//
//  Created by saurabh-pc on 09/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {
    var stringmode:String?
    
    @IBOutlet weak var loadWeb: UIWebView!
    override func viewDidLoad() {
        var url:URL?
       
        super.viewDidLoad()
        if stringmode=="webservice" {
            url = URL (string: "https://bitbucket.org/saurabhs70/ios-swift-and-ios_orm_based_restapi-swift")
            
        }
        else if stringmode=="sqlite" {
            url = URL (string: "https://bitbucket.org/saurabhs70/ios-swift-and-ios_orm_based_restapi-swift")
        }
        else {
            url = URL (string: "https://bitbucket.org/saurabhs70/ios-swift-and-ios_orm_based_restapi-swift")
        }

        let requestObj = URLRequest(url: url!)
        loadWeb.loadRequest(requestObj)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
