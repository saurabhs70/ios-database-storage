//
//  Icon.swift
//  Cruaapp
//
//  Created by saurabh-pc on 09/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class Icon: NSObject {

    var stringname:String?
    var stringImg:String?
    
   init(stringofname:String,stringImage:String) {
        stringname=stringofname
        stringImg=stringImage
    }
    
    func setup() -> Array<Any>? {
        var arr=[Any]()
        let  arrofimg = ["1lbshastri","atal","dhoni","indira","jln","klam","kohli","modi","sachinp","sandeep"]
        let  arrofname = ["Lal bhadur Shastri","Atal ji","Dhoni","Indira ji","Jwahar lal","Kalam ji","kohli","Modi ji","Sachin","Sandeep"]
        for i in 0 ..< arrofname.count
        {
            let imgobj:Icon=Icon.init(stringofname: arrofname[i], stringImage: arrofimg[i])
            arr.append(imgobj)
        }
        return arr
    }
    
}
