//
//  Minister+CoreDataProperties.swift
//  Cruaapp
//
//  Created by saurabh-pc on 05/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import Foundation
import CoreData


extension Minister {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Minister> {
        return NSFetchRequest<Minister>(entityName: "Minister");
    }

    @NSManaged public var mabout: String?
    @NSManaged public var mid: Int16
    @NSManaged public var mimage: String?
    @NSManaged public var mname: String?
    
//    override public func setValue(_ value: Any?, forKey key: String) {
//        
//    }


}
