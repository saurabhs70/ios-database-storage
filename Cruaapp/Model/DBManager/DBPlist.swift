//
//  DBPlist.swift
//  Cruaapp
//
//  Created by saurabh-pc on 05/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class DBPlist: NSObject {

    static let sharedInstance=DBPlist()
    func loadData() -> Array<Any>? {
        var arr=[AnyObject]()
        //arr.append("kkkl" as AnyObject)
        let path:String = getPath(stringPath: "Minister")
        
        let array = NSDictionary(contentsOfFile: path)
        let  arr2:Array<Any>=(array?.object(forKey: "values") as! Array<Any>?)!

        for stringv in arr2 {
            //let objminister:Minister = DBManager.sharedInstance

            var objministerId:Int16?
            var objministerName:String?
            var objministerAbout:String?
            var objministerImg:String?

            
            for (key,value) in stringv as! Dictionary<String, AnyObject> {
             //   print("\(key): \(value)")
                if key=="mid" {
                  objministerId=value as? Int16
                }
                if key=="mimage" {
                    objministerImg=value as? String

                }
                if key=="mname" {
                    objministerName=value as? String

                }
                if key=="mabout" {
                    objministerAbout=value as? String//value as? String

                }

                
            }
            let objminister:Minister=DBManager.sharedInstance.ministerWithInfo(stringName: objministerName!, stringDescription: objministerAbout!, stringid: objministerId!, stringimg: objministerImg!)
            //let objminister:Minister=DBManager.sharedInstance.addToMinister(stringName: <#T##String#>, stringDescription: <#T##String#>)
        arr.append(objminister)
       // print("\(stringv)")
        }
        
       // addToDatabase(stringname: "", stringinfo: "")
        return arr
        
    }
    func loadDataplist() -> Array<Any>? {
        var arr:Array<Any>?
        //arr.append("kkkl" as AnyObject)
        let path:String = getPath(stringPath: "Minister")
        
        let array = NSDictionary(contentsOfFile: path)
        arr=(array?.object(forKey: "values") as! Array<Any>?)!
        return arr
        
    }
    func getPath(stringPath:String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as! String
        let path = documentDirectory.appending("\(stringPath).plist")
        return path
    }
    func addToDatabase(stringname:String,stringinfo:String,stringimgnam:String) -> Bool {
        var arrlist:Array<Any>?
        
        arrlist=loadDataplist()
        var dictsave=[String:AnyObject]()
        dictsave["mid"] = DBManager.sharedInstance.pkid() as  AnyObject?
        dictsave["mimage"] = stringimgnam as AnyObject?
        dictsave["mname"] = stringname as AnyObject?
        dictsave["mabout"] = stringinfo as AnyObject?
    // dictsave?.setValue("pr", forKey: "mid")
       // dictsave?.setValue("adadadadad", forKey: "madd")
       // dictsave?.setValue("imgurl", forKey: "mimg")
        arrlist?.append(dictsave)
       // print(arrlist!);
        let boolv = writefile(arr: arrlist!)
        if boolv {
            return true
            //let arr=loadData()
           // print(arr);
        }
        else
        {
            return false
        }
        
        //return true
    }
    func updateList(stringname:String,ministerobj:Minister,stringaddress:String) ->Bool {
        var arrlist=[AnyObject]()
        
        arrlist=loadDataplist()! as [AnyObject]
        
        let index = arrlist.index(where: { dictionary in
            guard let value = dictionary["mid"] as? Int16
                else
            {
                return false
            }
            return value == ministerobj.mid
        })
        print( (index) ?? 67677676767)
        
        var dictsave = [String:Any]()
        dictsave["mid"] = ministerobj.mid
        dictsave["mimage"] = ministerobj.mimage
        dictsave["mname"] = stringname
        dictsave["mabout"] = stringaddress
arrlist[index!]=dictsave as AnyObject
        let reftesh = writefile(arr: arrlist)
        if reftesh {
            return true
        }
        return false

    }
    func deleteList(intId:Int16) ->Bool {
        var arrlist=[AnyObject]()
        
        arrlist=loadDataplist()! as [AnyObject]
        
                
//    
        let index = arrlist.index(where: { dictionary in
            guard let value = dictionary["mid"] as? Int16
                else
            {
                    return false
            }
            return value == intId
        })
        print( (index) ?? 67677676767)
        arrlist.remove(at: index!)
       let reftesh = writefile(arr: arrlist)
        if reftesh {
            return true
        }
        return false
    }
    func writefile(arr:Array<Any>) -> Bool {
        let path:String = getPath(stringPath: "Minister")

        if let dict = NSMutableDictionary(contentsOfFile: path) {
            //ToolKit.log(dict)
            dict.setValue(arr, forKey: "values")
            
            dict.write(toFile: path, atomically: false)
            return true;
    }
        else
        {
            return false;
        }
        
    }
}

/*
 if var dict = NSMutableDictionary(contentsOfFile: path!) {
 ToolKit.log(dict)
 dict.setValue("blablabla", forKey: "keyInPlist")
 
 dict.writeToFile(path!, atomically: false)
 }
*/
