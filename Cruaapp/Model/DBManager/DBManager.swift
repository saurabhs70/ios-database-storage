//
//  DBManager.swift
//  Cruaapp
//
//  Created by saurabh-pc on 05/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit
import CoreData
class DBManager: NSObject {

    static let sharedInstance = DBManager()
    
    //MARK:- Get all list
    func getLists(nameofenity:String) -> Array<Any>? {
    
        var arrlist:Array<Any>?
        
        
        let context=getContext() as NSManagedObjectContext
        context.reset()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:nameofenity)
        fetchRequest.returnsObjectsAsFaults=false

        do
        {
            arrlist=try context.fetch(fetchRequest)
            return arrlist
        }
        catch
        {
            return nil
        }
       // return arrlist
    }
    //MARK:- Add to model minister
    func addToMinister(stringName:String,stringDescription:String,stringimg:String) -> Bool {
        if stringName.characters.count>=0 && stringDescription.characters.count>=0 {
            
        
        let context = getContext() as NSManagedObjectContext
        let newminiter = NSEntityDescription.insertNewObject(forEntityName: "Minister", into: context)
        newminiter.setValue(stringName, forKey: "mname")
         newminiter.setValue(stringimg, forKey: "mimage")
        newminiter.setValue(stringDescription, forKey: "mabout")
        newminiter.setValue(pkid(), forKey: "mid")
        
        do
        {
            try context.save()
            return true
        }
        catch
        {
            return false

        }
        }
        else
        {
            return false;
        }
    }
    //MARK:- update model
    func updateMinister(ministerObj:Minister,stringname:String,stringabout:String) -> Bool {
        let intval:Int16 = ministerObj.mid as Int16

        let context = getContext() as NSManagedObjectContext
        let fetchreq = NSFetchRequest<NSFetchRequestResult>(entityName:"Minister")
        fetchreq.returnsObjectsAsFaults=false
        fetchreq.predicate=NSPredicate(format: "mid=%d",intval)
        do
        {
            let test = try context.fetch(fetchreq)
            if test.count>=1 {
                let ministerObjNew:Minister = test[0] as! Minister
                ministerObjNew.mabout=stringabout
                ministerObjNew.mname=stringname
                do
                {
                    try context.save()
                        return true
                
                }
                catch
                {
                    return false
                }
                
            }
        }
        catch
        {
            
        }
        
        return true
    }
    
    func deleteFromModel(ministerObj:Minister) -> Bool {
        let context = getContext() as NSManagedObjectContext
        let fetchreq = NSFetchRequest<NSFetchRequestResult>(entityName:"Minister")
        fetchreq.returnsObjectsAsFaults=false;
        fetchreq.predicate=NSPredicate(format: "mid=%d",ministerObj.mid )
        do
        {
            let finddata=try context.fetch(fetchreq)
            if finddata.count>=1 {
                let ministertodel:Minister=finddata[0] as! Minister
                context.delete(ministertodel)
                do
                {
                    try context.save()
                    return true
                }
                catch
                {
                    return false
                }
            }
            else
            {
                return false
            }
            
        }
        catch
        {
            return false
        }
        
        //return false
        
    }
    
    //MARK:- Create context
    func getContext() -> NSManagedObjectContext {
        
     let appdel=UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        return context
        
    }
    //MARK:- Create random int
    func pkid() -> Int16 {
        let randomNum:UInt16 = UInt16(arc4random_uniform(10000)) // range is 0 to 99
        
        // convert the UInt32 to some other  types
        
        // let randomTime:TimeInterval = TimeInterval(randomNum)
        
        let someInt16:Int = Int(Int16(randomNum))
        return Int16(someInt16)
    }
    //MARK:- Create custom model
    func ministerWithInfo(stringName:String,stringDescription:String,stringid:Int16,stringimg:String) -> Minister {
        let context = getContext() as NSManagedObjectContext
        let newminiter:Minister = NSEntityDescription.insertNewObject(forEntityName: "Minister", into: context) as! Minister
        newminiter.setValue(stringName, forKey: "mname")
        newminiter.setValue(stringDescription, forKey: "mabout")
        newminiter.setValue(stringid, forKey: "mid")
        newminiter.setValue(stringimg, forKey: "mimage")
    // context.reset()
   // context.delete(newminiter)
       //context.rollback()
        return newminiter as Minister;
     


    }
}
