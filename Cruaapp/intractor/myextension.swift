//
//  myextension.swift
//  Cruaapp
//
//  Created by saurabh-pc on 09/09/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView
{
    func circleImg(uiimg:UIImageView,stringname:String)->UIImageView {
        uiimg.layer.cornerRadius = uiimg.frame.size.width/2
        uiimg.clipsToBounds = true
        uiimg.image=UIImage(named:stringname)

        return uiimg
    }
}
